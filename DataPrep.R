
library(rgdal)
library(rgeos)
#observations





#SOS sites
SOS<-readOGR("/Users/daisy/GoogleDrive/Projects/OEHProtectedSpecies/Data/OEHManagmentSites/data/SiteManagedSpecies03112017/SearchResults.shp")
#get a list of all the species there are shapes for
spSOS<-SOS[SOS$SciName=="Crinia sloanei",]

#Map of NSW
AUS<-readOGR("/Users/daisy/GoogleDrive/PhD/Data/Spatial/AustraliaPolygon/STE11aAust.shp")
NSW<-AUS[AUS$STATE_NAME=="New South Wales",]
NSW <- gSimplify(NSW, tol=0.05, topologyPreserve=TRUE)

